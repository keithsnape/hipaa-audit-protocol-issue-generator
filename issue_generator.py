#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv
import gitlab

def create_issue():
    # Add your Access Token here for Personal Access Token authorization
    gl = gitlab.Gitlab('https://gitlab.com', private_token='')

    # Add your project ID here
    project_id =''

    project = gl.projects.get(project_id)

    # Opens the CSV and creates an issue for every row
    with open('hipaa-audit-protocol.csv') as f:
        reader = csv.reader(f)
        for col in reader:

            # Assigning columns to variables for demonstrative clarity
            audit_type = col[0]
            section = col[1]
            key_activity = col[2]
            established_performance_criteria = col[3]
            audit_inquiry = col[4]
            required_addressable = col[5]

            # Make the API call to create an issue with the data from the CSV
            issue = project.issues.create({'title': key_activity,
                                           'description': '## Established Performance Criteria' + '\n' +
                                            established_performance_criteria + '\n' + '## Audit Inquiry' +
                                             '\n' + audit_inquiry,
                                           'labels': [audit_type] + [required_addressable] + [section]})

try:
    create_issue()
except KeyboardInterrupt:
    print('Script was cancelled. Stopping issue creation.')
except Exception as error:
    print(error)
